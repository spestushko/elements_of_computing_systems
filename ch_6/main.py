from enum import Enum


class CommandType(Enum):
    A_COMMAND = 0
    C_COMMAND = 1
    L_COMMAND = 2


class Command:
    def __init__(self):
        self.type = None
        self.symbol = ''
        self.dest = 'null'
        self.comp = '0'
        self.jump = 'null'


class SymbolTable:
    def __init__(self):
        self._table = {
            'SP': 0,
            'LCL': 1,
            'ARG': 2,
            'THIS': 3,
            'THAT': 4,
            'R0': 0,
            'R1': 1,
            'R2': 2,
            'R3': 3,
            'R4': 4,
            'R5': 5,
            'R6': 6,
            'R7': 7,
            'R8': 8,
            'R9': 9,
            'R10': 10,
            'R11': 11,
            'R12': 12,
            'R13': 13,
            'R14': 14,
            'R15': 15,
            'SCREEN': 16384,
            'KBD': 24576

        }
        self.addr_avail = 16

    def _add_entry(self, symbol: str, address: int) -> None:
        self._table[symbol] = address

    def add_entry(self, symbol: str) -> None:
        if self.addr_avail > 24576:
            raise MemoryError('Memory index out of bounds')
        self._add_entry(symbol, self.addr_avail)
        self.addr_avail += 1

    def contains(self, symbol: str) -> bool:
        return symbol in self._table

    def get_address(self, symbol: str) -> int:
        return self._table[symbol] if self.contains(symbol) else None


class Parser:
    def __init__(self, fp):
        self._fp = fp
        self._code_lines = Parser._load_code_content(self._fp)
        self._line_num = 0
        self._current_command = None

    @staticmethod
    def _load_code_content(fp):
        code_lines = []
        with open(fp) as asm_file:
            line = asm_file.readline()
            while line:
                comment_pos = line.find('//')
                if comment_pos != -1:
                    line = line[0:comment_pos]
                code_line = line.strip()
                if code_line:
                    code_lines.append(code_line)
                line = asm_file.readline()
        return code_lines

    def has_more_commands(self) -> bool:
        return self._line_num < len(self._code_lines)

    def advance(self) -> None:
        self._line_num += 1 if self.has_more_commands() else 0

    def command_type(self) -> CommandType:
        command = self._code_lines[self._line_num]
        self._current_command = Command()
        addr_mnem_present = command.find('@') != -1
        cmd_mnem_present = command.find('=') != -1
        jmp_mnem_present = command.find(';') != -1
        if addr_mnem_present:
            addr_pos = command.find('@')
            self._current_command.type = CommandType.A_COMMAND
            self._current_command.symbol = command[addr_pos + 1:]
        elif cmd_mnem_present or jmp_mnem_present:
            self._current_command.type = CommandType.C_COMMAND
            eq_pos = command.find('=')
            jmp_pos = command.find(';')
            if cmd_mnem_present:
                self._current_command.dest = command[:eq_pos]
                self._current_command.comp = command[eq_pos + 1:len(command) if not jmp_mnem_present else jmp_pos]
            if jmp_mnem_present:
                self._current_command.comp = command[:jmp_pos]
                self._current_command.jump = command[jmp_pos+1:]
        else:
            self._current_command.type = CommandType.L_COMMAND
            br_st_pos, br_en_pos = command.find('('), command.find(')')
            self._current_command.symbol = command[br_st_pos+1:br_en_pos]
        return self._current_command.type

    def symbol(self) -> str:
        return self._current_command.symbol if self._current_command and self._current_command.type in [CommandType.A_COMMAND, CommandType.L_COMMAND] else ''

    def dest(self) -> str:
        return self._current_command.dest if self._current_command and self._current_command.type in [CommandType.C_COMMAND] else 'null'

    def comp(self) -> str:
        return self._current_command.comp if self._current_command and self._current_command.type in [CommandType.C_COMMAND] else '0'

    def jump(self) -> str:
        return self._current_command.jump if self._current_command and self._current_command.type in [CommandType.C_COMMAND] else 'null'


class Code:
    def __init__(self):
        self._dest_mnem_map = {
            'NULL': '000',
            'M':    '001',
            'D':    '010',
            'MD':   '011',
            'A':    '100',
            'AM':   '101',
            'AD':   '110',
            'AMD':  '111'
        }
        self._jump_mnem_map = {
            'NULL': '000',
            'JGT':  '001',
            'JEQ':  '010',
            'JGE':  '011',
            'JLT':  '100',
            'JNE':  '101',
            'JLE':  '110',
            'JMP':  '111'
        }
        self._comp_mnem_map = {
            '0':   '0101010',
            '1':   '0111111',
            '-1':  '0111010',
            'D':   '0001100',
            'A':   '0110000',
            '!D':  '0001101',
            '!A':  '0110001',
            '-D':  '0001111',
            '-A':  '0110011',
            'D+1': '0011111',
            'A+1': '0110111',
            'D-1': '0001110',
            'A-1': '0110010',
            'D+A': '0000010',
            'D-A': '0010011',
            'A-D': '0000111',
            'D&A': '0000000',
            'D|A': '0010101',
            'M':   '1110000',
            '!M':  '1110001',
            '-M':  '1110011',
            'M+1': '1110111',
            'M-1': '1110010',
            'D+M': '1000010',
            'D-M': '1010011',
            'M-D': '1000111',
            'D&M': '1000000',
            'D|M': '1010101'
        }

    def dest(self, mnemonic: str) -> str:
        return self._dest_mnem_map[mnemonic.upper()]

    def comp(self, mnemonic: str) -> str:
        return self._comp_mnem_map[mnemonic.upper()]

    def jump(self, mnemonic: str) -> str:
        return self._jump_mnem_map[mnemonic.upper()]


class Assembler:
    def __init__(self):
        self._code = Code()
        self._symbol_table = SymbolTable()

    def _first_pass(self, fp):
        parser = Parser(fp)
        rom_addr = 0
        while parser.has_more_commands():
            rom_addr += 1 if parser.command_type() in [CommandType.A_COMMAND, CommandType.C_COMMAND] else 0
            if parser.command_type() == CommandType.L_COMMAND:
                self._symbol_table._add_entry(parser.symbol(), rom_addr)
            parser.advance()

    def parse(self, fp):
        self._first_pass(fp)
        bin_commands = []
        parser = Parser(fp)
        while parser.has_more_commands():
            if parser.command_type() == CommandType.A_COMMAND:
                symbol = parser.symbol()
                if not symbol.isnumeric():
                    if not self._symbol_table.contains(symbol):
                        self._symbol_table.add_entry(symbol)
                    symbol = self._symbol_table.get_address(symbol)
                bin_commands.append('{0:016b}'.format(int(symbol)))
            elif parser.command_type() == CommandType.C_COMMAND:
                comp = self._code.comp(parser.comp())
                dest = self._code.dest(parser.dest())
                jump = self._code.jump(parser.jump())
                bin_commands.append(f'111{comp}{dest}{jump}')
            parser.advance()
        return '\n'.join(bin_commands)


def save_to_hack(filename, content):
    with open(f'resources/{filename}.hack', 'w') as hack_file:
        hack_file.write(content)


if __name__ == '__main__':
    asm = Assembler()
    parsed = asm.parse('resources/Pong.asm')
    save_to_hack('Pong', parsed)
