#/bin/bash
echo "... Staging generated assembler files"

cp resources/StaticTest.asm ../../../GitHub/nand2tetris/projects/07/MemoryAccess/StaticTest
cp resources/StackTest.asm ../../../GitHub/nand2tetris/projects/07/StackArithmetic/StackTest
cp resources/SimpleFunction.asm ../../../GitHub/nand2tetris/projects/08/FunctionCalls/SimpleFunction
cp resources/SimpleAdd.vm ../../../GitHub/nand2tetris/projects/07/StackArithmetic/SimpleAdd
cp resources/PointerTest.asm ../../../GitHub/nand2tetris/projects/07/MemoryAccess/PointerTest
cp resources/FibonacciSeries.asm ../../../GitHub/nand2tetris/projects/08/ProgramFlow/FibonacciSeries
cp resources/BasicTest.asm ../../../GitHub/nand2tetris/projects/07/MemoryAccess/BasicTest
cp resources/BasicLoop.asm ../../../GitHub/nand2tetris/projects/08/ProgramFlow/BasicLoop
cp resources/StaticsTest/StaticsTest.asm ../../../GitHub/nand2tetris/projects/08/FunctionCalls/StaticsTest
cp resources/FibonacciElement/FibonacciElement.asm ../../../GitHub/nand2tetris/projects/08/FunctionCalls/FibonacciElement

echo "... Staging completed"