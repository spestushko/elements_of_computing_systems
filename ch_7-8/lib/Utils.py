import os, glob


class Utils:
    @staticmethod
    def get_vm_files(fp):
        vm_files = []
        if os.path.isdir(fp):
            vm_files.extend(glob.glob(f'{fp}/*.vm'))
        elif os.path.isfile(fp):
            vm_files.append(fp)
        else:
            raise ValueError('-f argument must be: [a folder containing .vm files ; .vm file]')
        return vm_files
