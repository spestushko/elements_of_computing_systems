from enum import Enum


class Segment(Enum):
    ARGUMENT = 'argument'
    LOCAL = 'local'
    STATIC = 'static'
    CONSTANT = 'constant'
    THIS = 'this'
    THAT = 'that'
    POINTER = 'pointer'
    TEMP = 'temp'

    @staticmethod
    def list():
        return list(map(lambda c: c.value, Segment))
