from enum import Enum


class SpecialSymbols(Enum):
    SP = 'SP'
    LCL = 'LCL'
    ARG = 'ARG'
    THIS = 'THIS'
    THAT = 'THAT'
    TEMP_5 = '5'
    TEMP_6 = '6'
    TEMP_7 = '7'
    TEMP_8 = '8'
    TEMP_9 = '9'
    TEMP_10 = '10'
    TEMP_11 = '11'
    TEMP_12 = '12'
    R_13 = 'R13'
    R_14 = 'R14'
    R_15 = 'R15'

    @staticmethod
    def list():
        return list(map(lambda c: c.value, SpecialSymbols))
