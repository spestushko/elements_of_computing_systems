import argparse

from parser import Parser, CodeWriter
from lib import CommandType, Utils


def args_handler():
    parser = argparse.ArgumentParser(description='VM translator input sources')
    parser.add_argument('-f', dest='fp', default='resources/StaticsTest', action='store', help='File path of single file or directory')
    return parser.parse_args()


def parse(fp: str, code_writer: CodeWriter):
    vm_parser = Parser(fp)
    code_writer.set_current_file(fp.split('/')[-1].split('.')[0])
    while vm_parser.next():
        command = vm_parser.command()
        if command.command_type == CommandType.C_PUSH or command.command_type == CommandType.C_POP:
            code_writer.write_push_pop(command.command_type, command.arg1, command.arg2)
        elif command.command_type == CommandType.C_ARITHMETIC:
            code_writer.write_arithmetic(command.action)
        elif command.command_type == CommandType.C_LABEL:
            code_writer.write_label(command.arg1)
        elif command.command_type == CommandType.C_GOTO:
            code_writer.write_goto(command.arg1)
        elif command.command_type == CommandType.C_IF:
            code_writer.write_if(command.arg1)
        elif command.command_type == CommandType.C_CALL:
            code_writer.write_call(command.arg1, command.arg2)
        elif command.command_type == CommandType.C_RETURN:
            code_writer.write_return()
        elif command.command_type == CommandType.C_FUNCTION:
            code_writer.write_function(command.arg1, command.arg2)
        else:
            raise ValueError(f'Command: {command.command_type} unsupported')


if __name__ == '__main__':
    args = args_handler()
    vm_files = Utils.get_vm_files(args.fp)
    code_writer = CodeWriter()
    if len(vm_files) > 1:
        code_writer.write_init()
    for vm_file in vm_files:
        parse(vm_file, code_writer)
    code_writer.save_to_file(f'{vm_files[0].split(".")[0]}' if len(vm_files) == 1 else f'{args.fp}/{args.fp.split("/")[-1]}')
    print('done')
