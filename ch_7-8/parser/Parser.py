from lib import CommandType, Command


class Parser:
    def __init__(self, fp_in: str):
        self._fp = fp_in
        self._code = Parser._load_code_content(self._fp)
        self._parser_at = -1
        self._current_command = None
        self._c_arg_map = {
            'push': CommandType.C_PUSH,
            'pop': CommandType.C_POP,
            'label': CommandType.C_LABEL,
            'goto': CommandType.C_GOTO,
            'if-goto': CommandType.C_IF,
            'function': CommandType.C_FUNCTION,
            'return': CommandType.C_RETURN,
            'call': CommandType.C_CALL,
            **dict.fromkeys(['add', 'sub', 'neg', 'eq', 'gt', 'lt', 'and', 'or', 'not'], CommandType.C_ARITHMETIC)
        }

    @staticmethod
    def _load_code_content(fp):
        lines = []
        with open(fp) as asm_file:
            line = asm_file.readline()
            while line:
                comment_pos = line.find('//')
                if comment_pos != -1:
                    line = line[0:comment_pos]
                code_line = line.strip()
                if code_line:
                    lines.append(code_line)
                line = asm_file.readline()
        return lines

    def has_more_commands(self) -> bool:
        return self._parser_at < len(self._code)

    def _init_command(self):
        if self._parser_at >= len(self._code):
            raise ValueError('EOF reached.')
        if self._current_command:
            return self._current_command

        command = Command()
        # code assumes that loading code content skipped all empty lines
        arguments = list(filter(None, self._code[self._parser_at].split(' ')))
        c_arg = arguments[0].lower()
        if c_arg not in self._c_arg_map:
            raise ValueError('Invalid command argument.')
        command.command_type = self._c_arg_map[c_arg]
        command.action = c_arg
        if len(arguments) >= 2:
            command.arg1 = arguments[1]
        if len(arguments) >= 3:
            command.arg2 = int(arguments[2])
        self._current_command = command
        return self._current_command

    def next(self):
        self._parser_at += 1
        self._current_command = None
        if not self.has_more_commands():
            return False
        self._init_command()
        return True

    def command(self) -> Command:
        return self._current_command

    def command_type(self) -> CommandType:
        return self._current_command.command_type

    def arg1(self) -> str:
        return self._current_command.arg1

    def arg2(self) -> int:
        return self._current_command.arg2
