import uuid
from lib import SpecialSymbols, Segment

DEBUG = False


class AssemblerGenerator:

    def __init__(self):
        self._assembler_code = ''
        self._current_file = ''
        self._current_function = ''

    def _get_label(self, label):
        # Labels defined inside function, must be prefixed with current function name
        return f'{self._current_function}${label}'

    def set_current_function_name(self, func_name):
        self._current_function = func_name

    def set_current_file(self, filename):
        self._current_file = filename

    def code_dump(self):
        return self._assembler_code.split('\n')

    def load_from_ram(self, symbol: SpecialSymbols, index: int):
        # Calculates offset using index + base address of the symbol (memory location symbol is pointing to)
        # Points A to the calculated address, then stores value at calculated address into D register
        self._assembler_code += f'''
            @{index}
            D=A
            @{symbol.value} 
            A=M
            A=D+A
            D=M
        '''

    def load_from_segment(self, symbol: SpecialSymbols, index: int):
        # Calculates offset using index + address of the symbol itself (memory location of the symbol)
        # Points A to the calculated address, then stores value at calculated address into D register
        self._assembler_code += f'''
            @{index}
            D=A
            @{symbol.value}
            D=D+A
            A=D
            D=M
        '''

    def load_from_static_segment(self, symbol: str, index: int):
        # Static is defined by current file and index: example: class.1, loads memory from defined label
        self._assembler_code += f'''
            @{symbol}.{index}
            D=M
        '''

    def load_to_segment(self, symbol: SpecialSymbols, index: int):
        # Stores value to be saved in segment into R13
        # Calculates address using memory location of the segment symbol + index offset, then stores it in R14
        # Saves value from R13 at address in R14
        self._assembler_code += f'''
            @{SpecialSymbols.R_13.value} 
            M=D
            @{index}
            D=A
            @{symbol.value}
            D=D+A
            @{SpecialSymbols.R_14.value}
            M=D
            @{SpecialSymbols.R_13.value}
            D=M
            @{SpecialSymbols.R_14.value}
            A=M
            M=D
        '''

    def load_to_static_segment(self, symbol: str, index: int):
        # Static is defined by current file and index: example: class.1, loads D into memory location of defined label
        self._assembler_code += f'''
            @{symbol}.{index}        
            M=D
        '''

    def load_to_ram(self, symbol: SpecialSymbols, index: int):
        # Stores value to be saved in segment into R13
        # Calculates address using base address of the segment symbol + index offset, then stores it in R14
        # Saves value from R13 at address in R14
        self._assembler_code += f'''
            @{SpecialSymbols.R_13.value}
            M=D
            @{index}
            D=A
            @{symbol.value}
            A=M
            A=D+A
            D=A
            @{SpecialSymbols.R_14.value} 
            M=D
            @{SpecialSymbols.R_13.value} 
            D=M
            @{SpecialSymbols.R_14.value} 
            A=M
            M=D
        '''

    def read_memory(self, segment: str, index: int):
        """
        Puts value at register[index] into D
        :param segment: Memory register containing the value
        :param index: Index of memory segment
        """
        # Constant segment is the only virtual segment, thus data is provided by VM implementation directly
        if segment == Segment.CONSTANT.value:
            self._assembler_code += f'''
                @{index}
                D=A
            '''
        # All other segments access memory of the target platform
        elif segment == Segment.LOCAL.value:
            self.load_from_ram(SpecialSymbols.LCL, index)
        elif segment == Segment.ARGUMENT.value:
            self.load_from_ram(SpecialSymbols.ARG, index)
        elif segment == Segment.THIS.value:
            self.load_from_ram(SpecialSymbols.THIS, index)
        elif segment == Segment.THAT.value:
            self.load_from_ram(SpecialSymbols.THAT, index)
        elif segment == Segment.TEMP.value:
            self.load_from_segment(SpecialSymbols.TEMP_5, index)
        elif segment == Segment.POINTER.value:
            self.load_from_segment(SpecialSymbols.THIS, index)
        elif segment == Segment.STATIC.value:
            self.load_from_static_segment(self._current_file, index)
        else:
            raise ValueError(f'Segment unsupported. Supported indexes: {[Segment.list()]}')

    def write_memory(self, segment: str, index: int):
        """
        Writes value of D into segment[index]
        :param segment: Memory segment containing the value
        :param index: Index of memory segment
        """
        # Constant segment is the only virtual segment, thus nothing needs to be stored into target machine's memory
        if segment == Segment.CONSTANT.value:
            self._assembler_code += ''
        # All other segments access memory of the target platform
        elif segment == Segment.LOCAL.value:
            self.load_to_ram(SpecialSymbols.LCL, index)
        elif segment == Segment.ARGUMENT.value:
            self.load_to_ram(SpecialSymbols.ARG, index)
        elif segment == Segment.THIS.value:
            self.load_to_ram(SpecialSymbols.THIS, index)
        elif segment == Segment.THAT.value:
            self.load_to_ram(SpecialSymbols.THAT, index)
        elif segment == Segment.TEMP.value:
            self.load_to_segment(SpecialSymbols.TEMP_5, index)
        elif segment == Segment.POINTER.value:
            self.load_to_segment(SpecialSymbols.THIS, index)
        elif segment == Segment.STATIC.value:
            self.load_to_static_segment(self._current_file, index)
        else:
            raise ValueError(f'Segment unsupported. Supported indexes: {[Segment.list()]}')

    def push(self):
        # Stores D into memory location SP is pointing to, then base address of SP is incremented by 1
        self._assembler_code += f'''
            @SP
            A=M
            M=D
            @SP
            M=M+1
        '''

    def pop(self):
        # Takes value of the top of the stack and saves to D, then base address of SP is decremented by 1
        # Value before the pop which used to be top of the stack, is still in memory, but will be overwritten by
        # next push and thus is no longer accessible
        self._assembler_code += f'''
            @SP
            M=M-1
            A=M
            D=M
        '''

    def neg_D(self):
        # Negates value in D
        self._assembler_code += f'''
            D=-D
        '''

    def not_D(self):
        # Binary not value in D
        self._assembler_code += f'''
            D=!D
        '''

    def set_register_to_D(self, symbol: SpecialSymbols):
        # Updates value of the segment defined by symbol to be equal to D
        self._assembler_code += f'''
            @{symbol.value}
            M=D
        '''

    def add_registers(self, symbol_a: SpecialSymbols, symbol_b: SpecialSymbols):
        # Takes value from symbol_a, stores it in D, then adds to it value from symbol_b
        self._assembler_code += f'''
            @{symbol_a.value} 
            D=M
            @{symbol_b.value}
            D=D+M
        '''

    def sub_registers(self, symbol_a: SpecialSymbols, symbol_b: SpecialSymbols):
        # Takes value from symbol_a, stores it in D, then subs from it value from symbol_b
        self._assembler_code += f'''
            @{symbol_a.value} 
            D=M
            @{symbol_b.value}
            D=D-M
        '''

    def is_D_eq_0(self):
        # Generates a random hash to be used part of the jump label
        # If D == 0, then D will be set to -1 (True)
        # If D != 0, then D will be set to 0 (False)
        gen_id = uuid.uuid4().hex
        self._assembler_code += f'''
            @SET_TRUE_deq_{gen_id}
            D;JEQ
            D=0
            @FIN_COMP_deq_{gen_id}
            0;JMP
            (SET_TRUE_deq_{gen_id})
            D=-1
            (FIN_COMP_deq_{gen_id})
        '''

    def is_D_gt_0(self):
        # Generates a random hash to be used part of the jump label
        # If D > 0, then D will be set to -1 (True)
        # If D <= 0, then D will be set to 0 (False)
        gen_id = uuid.uuid4().hex
        self._assembler_code += f'''
            @SET_TRUE_dgt_{gen_id}
            D;JGT
            D=0
            @FIN_COMP_dgt_{gen_id}
            0;JMP
            (SET_TRUE_dgt_{gen_id})
            D=-1
            (FIN_COMP_dgt_{gen_id})
        '''

    def is_D_lt_0(self):
        # Generates a random hash to be used part of the jump label
        # If D < 0, then D will be set to -1 (True)
        # If D >= 0, then D will be set to 0 (False)
        gen_id = uuid.uuid4().hex
        self._assembler_code += f'''
            @SET_TRUE_dlt_{gen_id}
            D;JLT
            D=0
            @FIN_COMP_dlt_{gen_id}
            0;JMP
            (SET_TRUE_dlt_{gen_id})
            D=-1
            (FIN_COMP_dlt_{gen_id})
        '''

    def set_D_label_addr(self, label: str):
        # Saves to D register address of the label
        self._assembler_code += f'''
            @{label}
            D=A
        '''

    def set_D_label_value(self, label: str):
        # Saves to D register memory value of the label
        self._assembler_code += f'''
            @{label}
            D=M
        '''

    def and_registers(self, symbol_a: SpecialSymbols, symbol_b: SpecialSymbols):
        # Takes value from symbol_a, stores it in D, then does logical AND with value from symbol_b
        self._assembler_code += f'''
            @{symbol_a.value}
            D=M
            @{symbol_b.value}
            D=D&M
        '''

    def or_registers(self, symbol_a: SpecialSymbols, symbol_b: SpecialSymbols):
        # Takes value from symbol_a, stores it in D, then does logical OR with value from symbol_b
        self._assembler_code += f'''
            @{symbol_a.value}
            D=M
            @{symbol_b.value}
            D=D|M
        '''

    def write_init(self):
        if DEBUG:
            print(f'DEBUG: [write_init] {locals()}, current_function={self._current_function}')

        # Makes SP base address to be 256 according to the convention
        self._assembler_code += f'''
            @256
            D=A
            @{SpecialSymbols.SP.value}
            M=D
        '''

    def write_label(self, label: str):
        if DEBUG:
            print(f'DEBUG: [write_label] {locals()}, current_function={self._current_function}')

        # Writes label using current function name to be part of label name (if current function name is set, def - '')
        self._assembler_code += f'''
            ({self._get_label(label)})
        '''

    def write_goto(self, label: str):
        if DEBUG:
            print(f'DEBUG: [write_goto] {locals()}, current_function={self._current_function}')

        # Jumps to label using current function name (if current function name is set, def - '')
        self._assembler_code += f'''
            @{self._get_label(label)}        
            0;JMP
        '''

    def write_if(self, label: str):
        if DEBUG:
            print(f'DEBUG: [write_if] {locals()}, current_function={self._current_function}')

        # Jumps to label when D is True, using current function name (if current function name is set, def - '')
        self._assembler_code += f'''
            @{self._get_label(label)}
            D;JNE
        '''

    def write_function(self, func_name: str):
        if DEBUG:
            print(f'DEBUG: [write_function] {locals()}, current_function={self._current_function}')

        # Writes function name label
        self._assembler_code += f'''
            ({func_name})
        '''

    def write_call(self, func_name: str, num_args: int):
        if DEBUG:
            print(f'DEBUG: [write_call] {locals()}, current_function={self._current_function}')

        # Pushes return address on stack
        return_label = f'return{func_name}{uuid.uuid4().hex}'
        self.set_D_label_addr(return_label)
        self.push()
        # Saves LCL of the calling function
        self.set_D_label_value(SpecialSymbols.LCL.value)
        self.push()
        # Saves ARG of the calling function
        self.set_D_label_value(SpecialSymbols.ARG.value)
        self.push()
        # Saves THIS of the calling functions
        self.set_D_label_value(SpecialSymbols.THIS.value)
        self.push()
        # Saves THAT of the calling function
        self.set_D_label_value(SpecialSymbols.THAT.value)
        self.push()
        # Reposition ARG
        self._assembler_code += f'''
            @{SpecialSymbols.SP.value}
            D=M
            @{num_args}
            D=D-A
            @5
            D=D-A
            @{SpecialSymbols.ARG.value}
            M=D
        '''
        # Reposition LCL
        self._assembler_code += f'''
            @{SpecialSymbols.SP.value}
            D=M
            @{SpecialSymbols.LCL.value}
            M=D
        '''
        # Transfer control to f
        self._assembler_code += f'''
            @{func_name} 
            0;JMP
        '''
        # Declare a label for the return-address
        self._assembler_code += f'''
            ({return_label})
        '''

    def write_return(self):
        if DEBUG:
            print(f'DEBUG: [write_return] {locals()}, current_function={self._current_function}')

        frame = SpecialSymbols.R_13.value
        ret = SpecialSymbols.R_14.value
        # FRAME is a temporary variable
        self._assembler_code += f'''
            @{SpecialSymbols.LCL.value}
            D=M
            @{frame}
            M=D
        '''
        # Put the return-address in a temp. var
        self._assembler_code += f'''
            @{frame}
            D=M
            @5
            D=D-A
            A=D
            D=M
            @{ret}
            M=D
        '''
        # Reposition the return value of the caller
        self.pop()
        self._assembler_code += f'''
            @{SpecialSymbols.ARG.value}
            A=M
            M=D
        '''
        # Restore SP of the caller
        self._assembler_code += f'''
            @{SpecialSymbols.ARG.value}
            D=M
            @1
            D=D+A
            @{SpecialSymbols.SP.value}
            M=D
        '''
        # Restore THAT of the caller
        self._assembler_code += f'''
            @{frame}
            D=M
            @1
            D=D-A
            A=D
            D=M
            @{SpecialSymbols.THAT.value}
            M=D
        '''
        # Restore THIS of the caller
        self._assembler_code += f'''
            @{frame}
            D=M
            @2
            D=D-A
            A=D
            D=M
            @{SpecialSymbols.THIS.value} 
            M=D
        '''
        # Restore ARG of the caller
        self._assembler_code += f'''
            @{frame}
            D=M
            @3
            D=D-A
            A=D
            D=M
            @{SpecialSymbols.ARG.value}
            M=D
        '''
        # Restore LCL of the caller
        self._assembler_code += f'''
            @{frame}
            D=M
            @4
            D=D-A
            A=D
            D=M
            @{SpecialSymbols.LCL.value}
            M=D
        '''
        # Goto return-address (in the caller's code)
        self._assembler_code += f'''
            @{ret} 
            A=M
            0;JMP
        '''
