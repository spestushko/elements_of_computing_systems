from lib import CommandType, ArithmeticCommand, SpecialSymbols, Segment
from .AssemblerGenerator import AssemblerGenerator


class CodeWriter:
    def __init__(self):
        self._assembler_generator = AssemblerGenerator()

    def set_current_file(self, filename):
        self._assembler_generator.set_current_file(filename)

    @staticmethod
    def _validate_command(command, valid_commands):
        if command not in valid_commands:
            raise ValueError(f'Invalid command. Command must be one of: {valid_commands}')

    def write_init(self):
        """
        Calls assembler generator to write code that effects VM initialization according to convention
        :return:
        """
        self._assembler_generator.write_init()
        self._assembler_generator.write_call('Sys.init', 0)

    def write_arithmetic(self, command: str):
        """
        Calls assembler generator to write code that is the translation of the given arithmetic command
        :param command: Arithmetic command given
        :return:
        :raises: ValueError if invalid arithmetic command is passed in
        """
        if command == ArithmeticCommand.ADD.value:
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_13)
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_14)
            self._assembler_generator.add_registers(SpecialSymbols.R_13, SpecialSymbols.R_14)
            self._assembler_generator.push()
        elif command == ArithmeticCommand.SUB.value:
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_13)
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_14)
            self._assembler_generator.sub_registers(SpecialSymbols.R_14, SpecialSymbols.R_13)
            self._assembler_generator.push()
        elif command == ArithmeticCommand.NEG.value:
            self._assembler_generator.pop()
            self._assembler_generator.neg_D()
            self._assembler_generator.push()
        elif command == ArithmeticCommand.EQ.value:
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_13)
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_14)
            self._assembler_generator.sub_registers(SpecialSymbols.R_13, SpecialSymbols.R_14)
            self._assembler_generator.is_D_eq_0()
            self._assembler_generator.push()
        elif command == ArithmeticCommand.GT.value:
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_13)
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_14)
            self._assembler_generator.sub_registers(SpecialSymbols.R_14, SpecialSymbols.R_13)
            self._assembler_generator.is_D_gt_0()
            self._assembler_generator.push()
        elif command == ArithmeticCommand.LT.value:
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_13)
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_14)
            self._assembler_generator.sub_registers(SpecialSymbols.R_14, SpecialSymbols.R_13)
            self._assembler_generator.is_D_lt_0()
            self._assembler_generator.push()
        elif command == ArithmeticCommand.AND.value:
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_13)
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_14)
            self._assembler_generator.and_registers(SpecialSymbols.R_13, SpecialSymbols.R_14)
            self._assembler_generator.push()
        elif command == ArithmeticCommand.OR.value:
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_13)
            self._assembler_generator.pop()
            self._assembler_generator.set_register_to_D(SpecialSymbols.R_14)
            self._assembler_generator.or_registers(SpecialSymbols.R_13, SpecialSymbols.R_14)
            self._assembler_generator.push()
        elif command == ArithmeticCommand.NOT.value:
            self._assembler_generator.pop()
            self._assembler_generator.not_D()
            self._assembler_generator.push()
        else:
            raise ValueError(f'Invalid arithmetic commands. List of valid commands: {ArithmeticCommand}')

    def write_push_pop(self, command: CommandType, segment: str, index: int):
        """
        Calls assembler generator to write assembly code that is the translation of the given command
        :param command: Command given - either C_PUSH or C_POP
        :param segment: Segment from/to which to load/save data respectively
        :param index: Memory position in the segment defined above
        :return:
        :raises: ValueError if command is neither C_PUSH nor C_POP
        """
        CodeWriter._validate_command(command, [CommandType.C_PUSH, CommandType.C_POP])
        if command == CommandType.C_PUSH:
            self._assembler_generator.read_memory(segment, index)
            self._assembler_generator.push()
        elif command == CommandType.C_POP:
            self._assembler_generator.pop()
            self._assembler_generator.write_memory(segment, index)
        else:
            raise ValueError('Invalid command. Commands allowed: PUSH, POP')

    def write_label(self, label):
        """
        Calls assembler generator to write assembly code that effects the label command
        :param label: label that can be jumped to
        :return:
        """
        self._assembler_generator.write_label(label)

    def write_goto(self, label):
        """
        Calls assembler generator to write assembly code that effects the goto command
        :param label: label to jump to
        :return:
        """
        self._assembler_generator.write_goto(label)

    def write_if(self, label):
        """
        Calls assembler generator to write assembly code that effects the if-goto command
        :param label: label to jump to if condition in top stack element is true
        :return:
        """
        self._assembler_generator.pop()
        self._assembler_generator.write_if(label)

    def write_call(self, func_name: str, num_args: int):
        """
        Calls assembler generator to write assembly code that effects the call command
        :param func_name: name of the function to be executed
        :param num_args: amount of arguments passed into a function (used to reposition ARG register)
        :return:
        """
        self._assembler_generator.write_call(func_name, num_args)

    def write_return(self):
        """
        Calls assembler generator to write assembly code that effects the return command
        :return:
        """
        self._assembler_generator.write_return()

    def write_function(self, func_name: str, num_args: int):
        """
        Calls assembler generator to write assembly code that effects the function command
        :param func_name: Name of the function for which definition is created
        :param num_args: Amount of arguments pushed to stack initialized to 0
        :return:
        """
        self._assembler_generator.set_current_function_name(func_name)
        self._assembler_generator.write_function(func_name)
        for _ in range(num_args):
            self.write_push_pop(CommandType.C_PUSH, Segment.CONSTANT.value, 0)
        self._assembler_generator.set_current_function_name('')

    def save_to_file(self, filename):
        with open(f'{filename}.asm', 'w') as file_out:
            for each_line in self._assembler_generator.code_dump():
                formatted = each_line.lstrip()
                if formatted:
                    file_out.write(formatted + '\n')
