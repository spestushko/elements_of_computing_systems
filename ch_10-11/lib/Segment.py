from enum import Enum


class Segment(Enum):
    ARGUMENT = 'argument'
    LOCAL = 'local'
    STATIC = 'static'
    CONSTANT = 'constant'
    THIS = 'this'
    THAT = 'that'
    POINTER = 'pointer'
    TEMP = 'temp'

    def __str__(self):
        return ','.join([e.value for e in Segment])