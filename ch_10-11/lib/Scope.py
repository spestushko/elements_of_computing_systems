from enum import Enum


class Scope(Enum):
    CLASS = 'class'
    SUBROUTINE = 'subroutine'

    def __str__(self):
        return ','.join([e.value for e in Scope])