from enum import Enum


class IdentifierKind(Enum):
    STATIC = 'static'
    FIELD = 'field'
    ARGUMENT = 'arg'
    VAR = 'var'

    def __str__(self):
        return ','.join([e.value for e in IdentifierKind])
