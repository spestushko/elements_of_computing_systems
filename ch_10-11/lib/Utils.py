import os
import glob


class Utils:
    @staticmethod
    def get_jack_files(fp: str) -> list:
        jack_files = []
        if os.path.isdir(fp):
            jack_files.extend(glob.glob(f'{fp}/*.jack'))
        elif os.path.isfile(fp):
            jack_files.append(fp)
        else:
            raise ValueError('-f argument must be: [a folder containing .jack files ; .jack file]')
        return jack_files

    @staticmethod
    def save_to_file(fp: str, generated: str, extension: str) -> None:
        with open(f'{fp}.{extension}', 'w') as file_out:
            file_out.write(generated)
