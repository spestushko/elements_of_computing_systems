from enum import Enum


class Command(Enum):
    ADD = 'add'
    SUB = 'sub'
    NEG = 'neg'
    EQ = 'eq'
    GT = 'gt'
    LT = 'lt'
    AND = 'and'
    OR = 'or'
    NOT = 'not'
    MUL = 'call Math.multiply 2'
    DIV = 'call Math.divide 2'

    def __str__(self):
        return ','.join([e.value for e in Command])

    @staticmethod
    def map_op(ch: str):
        return {
            '+': Command['ADD'],
            '-': Command['SUB'],
            '=': Command['EQ'],
            '>': Command['GT'],
            '<': Command['LT'],
            '&': Command['AND'],
            '|': Command['OR'],
            '*': Command['MUL'],
            '/': Command['DIV']
        }.get(ch)

    @staticmethod
    def map_un_op(ch: str):
        return {
            '-': Command['NEG'],
            '~': Command['NOT']
        }.get(ch)
