import re
from lib import TokenType, KeyWord


class JackTokenizer:

    SYMBOLS = '()[]{},;=.+-*/&|-<>~'

    @staticmethod
    def _remove_multi_line_comments(src: str) -> str:
        """
        Removes multi line comments from the passed in string
        :param src:
            string containing jack source code
        :return:
        """
        return re.sub(re.compile('/\*.*?\*/', re.DOTALL), '', src)

    @staticmethod
    def _remove_one_line_comments(src: str) -> str:
        """
        Removes one liner comments from the passed in string
        :param src:
            string containing jack source code
        :return:
        """
        return re.sub(re.compile('//.*?\n'), '', src)

    @staticmethod
    def _load_code_content(fp: str) -> str:
        """
        Removes all comments and white space from the input file
        Example of comments:
            // Comment to end of line
            /* Comment until closing */
            /** API documentation comment */
        :param fp:
            File containing jack source code
        :return:
            Line by line source code stored in a list
        """
        with open(fp, 'r') as asm_file:
            lines = asm_file.readlines()
        lines = [JackTokenizer._remove_one_line_comments(line).strip() for line in lines]
        code = JackTokenizer._remove_multi_line_comments(' '.join(lines))
        return code.strip()

    def __init__(self, src_file: str):
        """
        Reads the input file contents and gets ready to tokenize it.
        :param src_file:
            file containing jack source code
        """
        self._src_file = src_file
        self._src_code = JackTokenizer._load_code_content(self._src_file)
        if not self._src_code or len(self._src_code) < 1:
            raise ValueError('Compilation error: Source code is empty')
        self._current_token = None
        self._previous_token = None
        self._code_ptr = 0

    def _handle_token_type_mismatch(self, expected: TokenType) -> None:
        """
        Checks if current token type matches expectation of the caller, throws exception if there is mismatch
        :param expected:
            TokenType expected by the caller
        :return:
        """
        cur_token_type = self.token_type()
        if cur_token_type.value != expected.value:
            raise ValueError(f'Compilation error: Expected {expected.value}, got {cur_token_type.value}')

    def _skip_empty_space(self):
        """
        Skips empty space in the code, until next character is found
        :return:
        """
        while self._code_ptr < len(self._src_code) and self._src_code[self._code_ptr].isspace():
            self._code_ptr += 1

    def has_more_tokens(self) -> bool:
        """
        Do we have more tokens in the input?
        :return:
        """
        runner_idx = self._code_ptr
        while runner_idx < len(self._src_code):
            is_symbol = self._src_code[runner_idx] in JackTokenizer.SYMBOLS
            is_alphanum = self._src_code[runner_idx].isalnum()
            if is_symbol or is_alphanum:
                return True
            runner_idx += 1
        return False

    def next_token(self):
        """
        Gets next token
        :return:
            Token coming after the current one
        """
        backup_code_ptr = self._code_ptr
        backup_curr_token = self._current_token
        self.advance()
        next_token = self.get_token()
        self._code_ptr = backup_code_ptr
        self._current_token = backup_curr_token
        return next_token

    def advance(self) -> None:
        """
        Gets the next token from the input and makes it the current token. This method should only be called
        if has_more_tokens() is true. Initially there is no current token.
        :return:
        """
        if not self.has_more_tokens():
            raise LookupError('Compilation error: No tokens remaining')

        self._skip_empty_space()
        self._previous_token = self._current_token
        self._current_token = ''
        try:
            while self._code_ptr < len(self._src_code):
                self._current_token += self._src_code[self._code_ptr]
                # Treating strings as a special case, and jumping over to the last pos of matching dbl quotes
                if self._current_token == '"' and self._src_code.find('"', self._code_ptr + 1) != -1:
                    match_quote_pos = self._src_code.find('"', self._code_ptr + 1)
                    self._current_token = self._src_code[self._code_ptr:match_quote_pos + 1]
                    self._code_ptr = match_quote_pos
                self.token_type()
                self._code_ptr += 1
        except ValueError:
            self._current_token = self._current_token[:-1]

    def token_type(self) -> TokenType:
        """
        Returns the type of the current token.
        :return:
        """
        if not self._current_token:
            raise LookupError('Compilation error: current token is undefined')

        if len(self._current_token) == 1 and self._current_token in JackTokenizer.SYMBOLS:
            token_type = TokenType.SYMBOL
        elif self._current_token in KeyWord.list():
            token_type = TokenType.KEYWORD
        elif self._current_token.isnumeric() and 0 <= int(self._current_token) <= 32767:
            token_type = TokenType.INT_CONST
        elif self._current_token[0] == '"' and self._current_token[-1] == '"' \
                and '\n' not in self._current_token and '"' not in self._current_token[1:-1]:
            token_type = TokenType.STRING_CONST
        elif self._current_token.isidentifier():
            token_type = TokenType.IDENTIFIER
        else:
            raise ValueError(f'Compilation error: Invalid token - [{self._current_token}]')

        return token_type

    def key_word(self) -> str:
        """
        Should be called only when token_type() is KEYWORD.
        :return:
            Returns the keyword which is the current token.
        """
        self._handle_token_type_mismatch(TokenType.KEYWORD)
        return KeyWord[self._current_token.upper()].value

    def symbol(self) -> str:
        """
        Should be called only when token_type() is SYMBOL.
        :return:
            Returns the character which is the current token.
        """
        self._handle_token_type_mismatch(TokenType.SYMBOL)
        return str(self._current_token)[:1]

    def identifier(self) -> str:
        """
        Should be called only when token_type() is IDENTIFIER.
        :return:
            Returns the identifier which is the current token.
        """
        self._handle_token_type_mismatch(TokenType.IDENTIFIER)
        return str(self._current_token)

    def int_val(self) -> int:
        """
        Should be called only when token_type() is INT_CONST.
        :return:
            Returns the integer value of the current token.
        """
        self._handle_token_type_mismatch(TokenType.INT_CONST)
        return int(self._current_token)

    def string_val(self) -> str:
        """
        Should be called only when token_type() is STRING_CONST.
        :return:
            Returns the string value of the current token, without the double quotes.
        """
        self._handle_token_type_mismatch(TokenType.STRING_CONST)
        return str(self._current_token[1:-1])

    def get_token(self):
        """
        Returns current token depending on the token type
        :return:
        """
        return {
            TokenType.KEYWORD: self.key_word,
            TokenType.SYMBOL: self.symbol,
            TokenType.IDENTIFIER: self.identifier,
            TokenType.INT_CONST: self.int_val,
            TokenType.STRING_CONST: self.string_val
        }.get(self.token_type(), '====TOMBSTONE===')()

    def get_previous_token(self):
        """
        Returns previous token without any formatting that get_token does
        :return:
        """
        return self._previous_token
