import argparse
import traceback

from analyzer import JackAnalyzer


def args_handler():
    help_string = 'Filepath of single jack file or directory with multiple jack files'
    parser = argparse.ArgumentParser(description='Jack compiler input sources')
    parser.add_argument('-f', dest='fp', default='resources/source/Math', action='store', help=help_string)
    return parser.parse_args()


if __name__ == '__main__':
    args = args_handler()
    try:
        JackAnalyzer(args.fp).process()
    except Exception as e:
        print(f'Error occurred reading/analyzing source files: {repr(e)}')
        traceback.print_exc()
    print('done')
