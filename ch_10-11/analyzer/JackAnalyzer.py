from lib import Utils, TokenType
from engine import CompilationEngine, SymbolTable, VMWriter
from tokenizer import JackTokenizer


class JackAnalyzer:

    def __init__(self, sources):
        """
        Analyzer operates on a given source which is a single jack file or a directory containing multiple jack files
        :param sources:
        """
        self._jack_files = Utils.get_jack_files(sources)

    def process(self):
        """
        Breaks input stream into Jack language tokens as specified by jack grammar
        :return:
        """
        for jack_file in self._jack_files:
            print(f'>>> Analyzing {jack_file}')
            tokenizer = JackTokenizer(jack_file)
            symbol_table = SymbolTable()
            vm_writer = VMWriter(symbol_table)
            compiler = CompilationEngine(tokenizer, symbol_table, vm_writer)

            while tokenizer.has_more_tokens():
                tokenizer.advance()
                token = tokenizer.get_token()
                token_t = tokenizer.token_type()

                if token_t == TokenType.KEYWORD:
                    compiler.handle_keyword(token)
                else:
                    raise ValueError(f'Compilation error: Unexpected starter token type, expected keyword got {token_t.value}')

            Utils.save_to_file(jack_file.split('.jack')[0], compiler.compiled_vm_code(), 'vm')
            Utils.save_to_file(jack_file.split('.jack')[0], compiler.compiled_xml_struct(), 'xml')
            print(f'  ! Successfully compiled {jack_file} \n')
