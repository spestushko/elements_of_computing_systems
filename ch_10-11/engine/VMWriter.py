from lib import Segment, Command
from .SymbolTable import SymbolTable


# Todo: extend to hold functions information
class VMWriter:

    def __init__(self, symbol_table: SymbolTable):
        """
        Creates a new instance that will keep track of VM code is generated so far (Writing to file is handled in
        lib util file, which is different from the spec)
        """
        self._symbol_table = symbol_table
        self._class_name = ''
        self._vm_code = []

    def _write(self, code) -> None:
        """
        Abstracts away how code writing is handled in the data structure representing final VM code
        :param code:
        :return:
        """
        self._vm_code.append(code)

    def adjust_func_n_vars(self, name, n_locals):
        """
        Finds function name and modifies number of local variables passed in to be n_locals
        :param name:
        :param n_locals:
        :return:
        """
        expect_cmd = f'function {self._class_name}.{name} 0'
        target_cmd = f'function {self._class_name}.{name} {n_locals}'
        for i in range(len(self._vm_code)):
            if self._vm_code[i] == expect_cmd:
                self._vm_code[i] = target_cmd

    def write_push(self, segment: Segment, index: int) -> None:
        """
        Writes a VM push command
        :param segment: Memory segment
        :param index: Index in the memory segment
        :return:
        """
        self._write(f'push {segment.value} {index}')

    def write_pop(self, segment: Segment, index: int) -> None:
        """
        Writes a VM pop command
        :param segment: Memory segment
        :param index: Index in the memory segment
        :return:
        """
        self._write(f'pop {segment.value} {index}')

    def write_arithmetic(self, command: Command):
        """
        Writes a VM arithmetic command
        :param command:
        :return:
        """
        self._write(f'{command.value}')

    def write_label(self, label: str) -> None:
        """
        Writes a VM label command
        :param label: Name of the label to declare
        :return:
        """
        self._write(f'label {label}')

    def write_goto(self, label: str) -> None:
        """
        Writes a VM goto command
        :param label: Name of the label to pass control flow to
        :return:
        """
        self._write(f'goto {label}')

    def write_if(self, label: str) -> None:
        """
        Writes a VM if-goto command
        :param label: Name of the label to pass control flow to if condition is true
        :return:
        """
        self._write(f'if-goto {label}')

    def write_call(self, name: str, n_args: int) -> None:
        """
        Writes a VM call command
        :param name: Name of the function to call
        :param n_args: Number of arguments to be pushed onto the stack
        :return:
        """
        if '.' not in name:
            name = f'{self._class_name}.{name}'
        self._write(f'call {name} {n_args}')

    def write_function(self, name: str, n_locals: int) -> None:
        """
        Writes a VM function command
        :param name: Name of the function to declare
        :param n_locals: Number of local arguments declared in the local scope
        :return:
        """
        self._write(f'function {self._class_name}.{name} {n_locals}')

    def write_return(self):
        """
        Writes a VM return command
        :return:
        """
        self._write(f'return')

    def compiled(self):
        """
        Returns generated VM code (different from spec, replaces `close` method)
        :return:
        """
        formatted_code = ''
        for line in self._vm_code:
            formatted_code += f'{line.strip()}\n'
        return formatted_code

    def set_class_name(self, class_name: str):
        """
        Setter for current class_name being compiled
        :return:
        """
        self._class_name = class_name

    def get_class_name(self) -> str:
        """
        Getter for current class_name being compiled
        :return:
        """
        return self._class_name
