from typing import Callable
from lib import KeyWord, TokenType, IdentifierKind, Segment, Command
from enum import Enum
from html import escape
import uuid

from tokenizer import JackTokenizer
from .SymbolTable import SymbolTable
from .VMWriter import VMWriter


class CompilationEngine:

    _TOMBSTONE_ = '====TOMBSTONE===='

    TOKEN_VALUE = 0
    TOKEN_TYPE = 1

    class XmlKeys(Enum):
        VAR_DEC = 'varDec'
        CLASS_VAR_DEC = 'classVarDec'
        PARAM_LIST = 'parameterList'
        SUBROUTINE_BODY = 'subroutineBody'
        SUBROUTINE_DEC = 'subroutineDec'
        CLASS_DEC = 'class'
        EXPRESSION = 'expression'
        EXPRESSION_LIST = 'expressionList'
        STATEMENTS = 'statements'
        RETURN_STATEMENT = 'returnStatement'
        DO_STATEMENT = 'doStatement'
        LET_STATEMENT = 'letStatement'
        WHILE_STATEMENT = 'whileStatement'
        IF_STATEMENT = 'ifStatement'
        TERM = 'term'

    class Grammar(Enum):
        OP = [c for c in '+-*/&|<>=']
        UN_OP = [c for c in '-~']

    def __init__(self, tokenizer: JackTokenizer, symbol_table: SymbolTable, vm_writer: VMWriter):
        """
        Creates a new compilation engine with with given tokenizer. Next routine called must be compile_class().
        :param tokenizer:
        """
        self._symbol_table = symbol_table
        self._tokenizer = tokenizer
        self._vm_writer = vm_writer
        self._compiled_source = ''
        self._keyword_handlers = {
            **dict.fromkeys([KeyWord.STATIC, KeyWord.FIELD], self.compile_class_var_dec),
            **dict.fromkeys([KeyWord.CONSTRUCTOR, KeyWord.FUNCTION, KeyWord.METHOD], self.compile_subroutine),
            KeyWord.CLASS: self.compile_class,
            KeyWord.VAR: self.compile_var_dec,
            KeyWord.LET: self.compile_let,
            KeyWord.IF: self.compile_if,
            KeyWord.WHILE: self.compile_while,
            KeyWord.DO: self.compile_do,
            KeyWord.RETURN: self.compile_return
        }
        self._segment_of_kind = {
            IdentifierKind.VAR: Segment.LOCAL,
            IdentifierKind.ARGUMENT: Segment.ARGUMENT,
            IdentifierKind.FIELD: Segment.THIS,
            IdentifierKind.STATIC: Segment.STATIC
        }

    def _expect_token(self, allowed: list, handler: Callable) -> None:
        """
        Expects a token of specific type, and if condition is satisfied, calls a handler to compile a token
        :param allowed:
            List of allowed tokens
        :param handler:
            Function to handle token compilation
        :return:
        """
        if not self._tokenizer.has_more_tokens():
            raise ValueError(f'Compilation error: Expected tokens to compile, got None')
        token, token_t = self._token_type_pair()
        if token.lower() not in allowed:
            raise ValueError(f'Compilation error: Expected one of {allowed}, got {token_t.value} {token}')
        handler()

    def _token_type_pair(self):
        """
        Returns a pair of current token and token type for simplicity of use
        :return:
        """
        return self._tokenizer.get_token(), self._tokenizer.token_type()

    def _compile_raw(self, src):
        """
        Updates compiled source code with new code maintaining proper indentation
        :param code:
            new code
        :return:
        """
        self._compiled_source += f'{src}\n'

    def _compile_generic(self, escaped=False, metadata=None) -> None:
        """
        Compiles current token and symbol
        :return:
        """
        meta_context = metadata['context'] if metadata and 'context' in metadata else ''
        meta_type = metadata['type'] if metadata and 'type' in metadata else 'class/subroutine'
        meta_kind = metadata['kind'] if metadata and 'kind' in metadata else ''

        token, token_t = self._token_type_pair()
        token = escape(token) if escaped else token
        if token_t == TokenType.IDENTIFIER:
            s_type = self._symbol_table.type_of(token)
            kind = self._symbol_table.kind_of(token)
            index = self._symbol_table.index_of(token)

            s_type = s_type if s_type else meta_type
            kind = kind.value if kind else meta_kind

            self._compile_raw(f'<{token_t.value}> {token} | metadata: type=[{s_type}] context=[{meta_context}] kind=[{kind}] index=[{index}] </{token_t.value}>')
        else:
            self._compile_raw(f'<{token_t.value}> {token} </{token_t.value}>')

        if self._tokenizer.has_more_tokens():
            self._tokenizer.advance()

    def _compile_symbol(self) -> None:
        """
        Compiles individual symbols
        :return:
        """
        self._compile_generic(escaped=True)

    def _compile_op(self) -> None:
        """
        Compiles Operand symbol
        :return:
        """
        self._expect_token(CompilationEngine.Grammar.OP.value, self._compile_symbol)

    def _compile_unary_op(self) -> None:
        """
        Compiles unary operand symbol
        :return:
        """
        self._expect_token(CompilationEngine.Grammar.UN_OP.value, self._compile_symbol)

    def _compile_keyword_constant(self):
        """
        Compiles keyword constant
        :return:
        """
        expected_tokens = [KeyWord.TRUE.value, KeyWord.FALSE.value, KeyWord.NULL.value, KeyWord.THIS.value]
        if self._tokenizer.get_token().lower() in [KeyWord.FALSE.value, KeyWord.NULL.value]:
            self._vm_writer.write_push(Segment.CONSTANT, 0)
        elif self._tokenizer.get_token().lower() == KeyWord.TRUE.value:
            self._vm_writer.write_push(Segment.CONSTANT, 1)
            self._vm_writer.write_arithmetic(Command.NEG)
        elif self._tokenizer.get_token().lower() == KeyWord.THIS.value:
            self._vm_writer.write_push(Segment.POINTER, 0)
        self._expect_token(expected_tokens, self._compile_keyword)

    def _compile_identifier(self, metadata=None) -> None:
        """
        Compiles identifier
        :return:
        """
        self._compile_generic(metadata=metadata)

    def _compile_keyword(self) -> None:
        """
        Compiles keyword
        :return:
        """
        self._compile_generic()

    def _compile_integer_constant(self) -> None:
        """
        Compiles integer constant
        :return:
        """
        self._vm_writer.write_push(segment=Segment.CONSTANT, index=self._tokenizer.get_token())
        self._compile_generic()

    def _compile_string_constant(self) -> None:
        """
        Compiles string constant
        :return:
        """
        s_const = self._tokenizer.get_token()
        self._compile_generic()
        self._vm_writer.write_push(Segment.CONSTANT, len(s_const))
        self._vm_writer.write_call('String.new', 1)
        self._vm_writer.write_pop(Segment.TEMP, 0)
        for i in range(len(s_const)):
            self._vm_writer.write_push(Segment.TEMP, 0)
            self._vm_writer.write_push(Segment.CONSTANT, ord(s_const[i]))
            self._vm_writer.write_call('String.appendChar', 2)
            self._vm_writer.write_pop(Segment.TEMP, 0)
        self._vm_writer.write_push(Segment.TEMP, 0)

    def _compile_type(self) -> None:
        """
        Compiles variable type
        :return:
        """
        allowed_primitive_types = [KeyWord.INT.value, KeyWord.CHAR.value, KeyWord.BOOLEAN.value]
        token, token_t = self._token_type_pair()
        if token.lower() in allowed_primitive_types:
            self._compile_keyword()
        elif token_t == TokenType.IDENTIFIER:
            self._compile_class_name(metadata={'context': 'used', 'type': 'class'})
        else:
            raise ValueError(f'Compilation error: Expected [int, char, boolean, className],got {token_t.value} {token}')

    def _compile_class_name(self, metadata=None) -> None:
        """
        Compiles class name identifier
        :return:
        """
        self._compile_identifier(metadata=metadata)

    def _compile_subroutine_name(self, metadata=None) -> None:
        """
        Compiles subroutine name identifier
        :return:
        """
        self._compile_identifier(metadata=metadata)

    def _compile_var_name(self, metadata=None) -> None:
        """
        Compiles variable name identifier
        :return:
        """
        self._compile_identifier(metadata=metadata)

    def _compile_subroutine_body(self, f_name: str) -> None:
        """
        Compiles a subroutine body
        :return:
        """
        self._compile_raw(f'<{CompilationEngine.XmlKeys.SUBROUTINE_BODY.value}>')
        self._expect_token(['{'], self._compile_symbol)
        token, token_t = self._token_type_pair()
        while token != '}':
            if token.lower() == KeyWord.VAR.value:
                self.compile_var_dec()
            else:
                self.compile_statements()
            token, token_t = self._token_type_pair()
        self._compile_symbol()
        self._vm_writer.adjust_func_n_vars(f_name, self._symbol_table.var_count(IdentifierKind.VAR))
        self._compile_raw(f'</{CompilationEngine.XmlKeys.SUBROUTINE_BODY.value}>')

    def _compile_subroutine_call(self) -> None:
        """
        Compiles a subroutine(constructor, method, function) call
        :return:
        """
        subroutine_name = self._tokenizer.get_token()
        self._compile_identifier(metadata={'context': 'used'})
        token, token_t = self._token_type_pair()
        if token == '(':
            # If calling method it's implied that we are calling an instance of this
            self._vm_writer.write_push(Segment.POINTER, 0)
            self._compile_symbol()
            args = self.compile_expression_list()
            self._expect_token([')'], self._compile_symbol)
            self._vm_writer.write_call(name=subroutine_name, n_args=args+1)
        elif token == '.':
            # If calling method pass on stack the instance on which method is executed on
            self._compile_symbol()
            args = 0
            object_name = subroutine_name
            subroutine_name = self._tokenizer.get_token()
            segment = self._segment_of_kind.get(self._symbol_table.kind_of(object_name))
            if segment is not None:
                index = self._symbol_table.index_of(object_name)
                self._vm_writer.write_push(segment, index)
                args += 1
            self._compile_subroutine_name(metadata={'context': 'used', 'type': 'subroutine'})
            self._expect_token(['('], self._compile_symbol)
            args += self.compile_expression_list()
            self._expect_token([')'], self._compile_symbol)
            class_name = self._symbol_table.type_of(object_name) if self._symbol_table.type_of(object_name) is not None else object_name
            self._vm_writer.write_call(name=f'{class_name}.{subroutine_name}', n_args=args)
        else:
            raise ValueError(f'Compilation error: expected "(" or "." got {token_t.value}, {token}')

    def handle_keyword(self, token) -> None:
        """
        Calls compile method depending on the type of keyword in the argument
        :return:
        """
        kw_token = KeyWord[token.upper()]
        if kw_token not in self._keyword_handlers:
            raise ValueError(f'Compilation error: Unexpected keyword {token}')
        self._keyword_handlers[kw_token]()

    def compile_class(self) -> None:
        """
        Compiles a complete class
        :return:
        """
        self._compile_raw(f'<{CompilationEngine.XmlKeys.CLASS_DEC.value}>')
        self._expect_token([KeyWord.CLASS.value], self._compile_keyword)
        self._vm_writer.set_class_name(self._tokenizer.get_token())
        self._compile_class_name(metadata={'context': 'defined', 'type': 'class'})
        self._expect_token(['{'], self._compile_symbol)
        token, token_t = self._token_type_pair()
        while token != '}':
            if token.lower() in [KeyWord.STATIC.value, KeyWord.FIELD.value]:
                self.compile_class_var_dec()
            elif token.lower() in [KeyWord.CONSTRUCTOR.value, KeyWord.FUNCTION.value, KeyWord.METHOD.value]:
                self.compile_subroutine()
            else:
                raise ValueError(f'Compilation error: Expected class var or subroutine, got {token_t.value} {token}')
            token, token_t = self._token_type_pair()
        self._compile_symbol()
        self._compile_raw(f'</{CompilationEngine.XmlKeys.CLASS_DEC.value}>')

    def compile_class_var_dec(self) -> None:
        """
        Compiles a static declaration or a field declaration.
        :return:
        """
        self._compile_raw(f'<{CompilationEngine.XmlKeys.CLASS_VAR_DEC.value}>')
        symbol_kind = IdentifierKind[self._tokenizer.get_token().upper()]
        self._expect_token([KeyWord.STATIC.value, KeyWord.FIELD.value], self._compile_keyword)
        symbol_type = self._tokenizer.get_token()
        self._compile_type()
        symbol_name = self._tokenizer.get_token()
        self._symbol_table.define(name=symbol_name, s_type=symbol_type, kind=symbol_kind)
        self._compile_var_name(metadata={'context': 'defined'})
        token, token_t = self._token_type_pair()
        while token != ';':
            self._compile_symbol()
            symbol_name = self._tokenizer.get_token()
            self._symbol_table.define(name=symbol_name, s_type=symbol_type, kind=symbol_kind)
            self._compile_var_name(metadata={'context': 'defined'})
            token, token_t = self._token_type_pair()
        self._compile_symbol()
        self._compile_raw(f'</{CompilationEngine.XmlKeys.CLASS_VAR_DEC.value}>')

    def compile_subroutine(self) -> None:
        """
        Compiles a complete method, function or constructor.
        :return:
        """
        self._compile_raw(f'<{CompilationEngine.XmlKeys.SUBROUTINE_DEC.value}>')
        self._symbol_table.start_subroutine()
        sub_type = self._tokenizer.get_token()
        if sub_type.lower() == KeyWord.METHOD.value:
            self._symbol_table.define(name=CompilationEngine._TOMBSTONE_,
                                      s_type=CompilationEngine._TOMBSTONE_,
                                      kind=IdentifierKind.ARGUMENT)
        self._expect_token([KeyWord.CONSTRUCTOR.value, KeyWord.FUNCTION.value, KeyWord.METHOD.value], self._compile_keyword)
        token, _ = self._token_type_pair()
        self._compile_keyword() if token.lower() == KeyWord.VOID.value else self._compile_type()
        subroutine_name = self._tokenizer.get_token()
        self._compile_subroutine_name(metadata={'context': 'defined', 'type': 'subroutine'})
        self._vm_writer.write_function(subroutine_name, 0)
        self.compile_parameter_list()
        if sub_type.lower() == KeyWord.METHOD.value:
            self._vm_writer.write_push(Segment.ARGUMENT, 0)
            self._vm_writer.write_pop(Segment.POINTER, 0)
        elif sub_type.lower() == KeyWord.CONSTRUCTOR.value:
            self._vm_writer.write_push(Segment.CONSTANT, self._symbol_table.var_count(IdentifierKind.FIELD))
            self._vm_writer.write_call('Memory.alloc', 1)
            self._vm_writer.write_pop(Segment.POINTER, 0)
        self._compile_subroutine_body(subroutine_name)
        self._compile_raw(f'</{CompilationEngine.XmlKeys.SUBROUTINE_DEC.value}>')

    def compile_parameter_list(self) -> None:
        """
        Compiles a (possibly empty) parameter list, not including the enclosing "()".

        Note: different from spec, as brackets are handled as part of this method, rather
        than outside as described in the book.
        :return:
        """
        self._expect_token(['('], self._compile_symbol)
        self._compile_raw(f'<{CompilationEngine.XmlKeys.PARAM_LIST.value}>')
        token, _ = self._token_type_pair()
        while token != ')':
            symbol_type = token
            self._compile_type()
            symbol_name = self._tokenizer.get_token()
            self._symbol_table.define(name=symbol_name, s_type=symbol_type, kind=IdentifierKind.ARGUMENT)
            self._compile_var_name(metadata={'context': 'defined'})
            token, _ = self._token_type_pair()
            if token == ')':
                break
            self._expect_token([','], self._compile_symbol)
            token, token_t = self._token_type_pair()
        self._compile_raw(f'</{CompilationEngine.XmlKeys.PARAM_LIST.value}>')
        self._expect_token([')'], self._compile_symbol)

    def compile_var_dec(self) -> None:
        """
        Compiles a var declaration
        :return:
        """
        self._compile_raw(f'<{CompilationEngine.XmlKeys.VAR_DEC.value}>')
        self._expect_token([KeyWord.VAR.value], self._compile_keyword)
        symbol_type = self._tokenizer.get_token()
        self._compile_type()
        symbol_name = self._tokenizer.get_token()
        self._symbol_table.define(name=symbol_name, s_type=symbol_type, kind=IdentifierKind.VAR)
        self._compile_var_name(metadata={'context': 'defined'})
        token, token_t = self._token_type_pair()
        while token != ';':
            self._compile_symbol()
            symbol_name = self._tokenizer.get_token()
            self._symbol_table.define(name=symbol_name, s_type=symbol_type, kind=IdentifierKind.VAR)
            self._compile_var_name(metadata={'context': 'defined'})
            token, token_t = self._token_type_pair()
        self._compile_symbol()
        self._compile_raw(f'</{CompilationEngine.XmlKeys.VAR_DEC.value}>')

    def compile_statements(self) -> None:
        """
        Compiles a sequence of statements, not including the enclosing "{}".
        :return:
        """
        allowed_statements = [KeyWord.LET.value, KeyWord.IF.value, KeyWord.WHILE.value, KeyWord.DO.value, KeyWord.RETURN.value]
        self._compile_raw(f'<{CompilationEngine.XmlKeys.STATEMENTS.value}>')
        while self._tokenizer.get_token() in allowed_statements:
            self.handle_keyword(self._tokenizer.get_token())
        self._compile_raw(f'</{CompilationEngine.XmlKeys.STATEMENTS.value}>')

    def compile_do(self) -> None:
        """
        Compiles a do statement.
        :return:
        """
        self._compile_raw(f'<{CompilationEngine.XmlKeys.DO_STATEMENT.value}>')
        self._expect_token([KeyWord.DO.value], self._compile_keyword)
        self._compile_subroutine_call()
        self._expect_token([';'], self._compile_symbol)
        self._vm_writer.write_pop(Segment.TEMP, 0)
        self._compile_raw(f'</{CompilationEngine.XmlKeys.DO_STATEMENT.value}>')

    def compile_let(self) -> None:
        """
        Compiles a let statement.
        :return:
        """
        self._compile_raw(f'<{CompilationEngine.XmlKeys.LET_STATEMENT.value}>')
        self._expect_token([KeyWord.LET.value], self._compile_keyword)
        var_name = self._tokenizer.get_token()
        var_segment = self._segment_of_kind.get(self._symbol_table.kind_of(var_name))
        var_index = self._symbol_table.index_of(var_name)
        self._compile_var_name(metadata={'context': 'used'})
        token, _ = self._token_type_pair()
        is_array = False
        if token == '[':
            is_array = True
            self._compile_symbol()
            self.compile_expression()
            self._vm_writer.write_push(var_segment, var_index)
            self._vm_writer.write_arithmetic(Command.ADD)
            self._vm_writer.write_pop(Segment.TEMP, 1)
            self._expect_token([']'], self._compile_symbol)
        self._expect_token(['='], self._compile_symbol)
        self.compile_expression()
        self._expect_token([';'], self._compile_symbol)
        if is_array:
            self._vm_writer.write_push(Segment.TEMP, 1)
            self._vm_writer.write_pop(Segment.POINTER, 1)
            self._vm_writer.write_pop(Segment.THAT, 0)
            self._vm_writer.write_push(Segment.CONSTANT, 0)
            self._vm_writer.write_pop(Segment.TEMP, 1)
        else:
            self._vm_writer.write_pop(var_segment, var_index)
        self._compile_raw(f'</{CompilationEngine.XmlKeys.LET_STATEMENT.value}>')

    def compile_while(self) -> None:
        """
        Compiles a while statement.
        :return:
        """
        lbl_repeat = str(uuid.uuid4())
        lbl_end = str(uuid.uuid4())

        self._compile_raw(f'<{CompilationEngine.XmlKeys.WHILE_STATEMENT.value}>')
        self._expect_token([KeyWord.WHILE.value], self._compile_keyword)
        self._expect_token(['('], self._compile_symbol)
        self._vm_writer.write_label(lbl_repeat)
        self.compile_expression()
        self._vm_writer.write_arithmetic(Command.NOT)
        self._vm_writer.write_if(lbl_end)
        self._expect_token([')'], self._compile_symbol)
        self._expect_token(['{'], self._compile_symbol)
        self.compile_statements()
        self._vm_writer.write_goto(lbl_repeat)
        self._expect_token(['}'], self._compile_symbol)
        self._vm_writer.write_label(lbl_end)
        self._compile_raw(f'</{CompilationEngine.XmlKeys.WHILE_STATEMENT.value}>')

    def compile_return(self):
        """
        Compiles a return statement.
        :return:
        """
        void_func = True
        self._compile_raw(f'<{CompilationEngine.XmlKeys.RETURN_STATEMENT.value}>')
        self._expect_token([KeyWord.RETURN.value], self._compile_keyword)
        token, token_t = self._token_type_pair()
        if token != ';':
            void_func = False
            self.compile_expression()
        self._expect_token([';'], self._compile_symbol)
        if void_func:
            self._vm_writer.write_push(Segment.CONSTANT, 0)
        self._compile_raw(f'</{CompilationEngine.XmlKeys.RETURN_STATEMENT.value}>')
        self._vm_writer.write_return()

    def compile_if(self) -> None:
        """
        Compiles an if statement, possibly with a trailing else clause.
        :return:
        """
        lbl_else = str(uuid.uuid4())
        lbl_end = str(uuid.uuid4())

        self._compile_raw(f'<{CompilationEngine.XmlKeys.IF_STATEMENT.value}>')
        self._expect_token([KeyWord.IF.value], self._compile_keyword)
        self._expect_token(['('], self._compile_symbol)
        self.compile_expression()
        self._vm_writer.write_arithmetic(Command.NOT)
        self._vm_writer.write_if(lbl_else)
        self._expect_token([')'], self._compile_symbol)
        self._expect_token(['{'], self._compile_symbol)
        self.compile_statements()
        self._vm_writer.write_goto(lbl_end)
        self._expect_token(['}'], self._compile_symbol)

        self._vm_writer.write_label(lbl_else)
        if self._tokenizer.get_token().upper() == 'ELSE':
            self._compile_keyword()
            self._expect_token(['{'], self._compile_symbol)
            self.compile_statements()
            self._expect_token(['}'], self._compile_symbol)

        self._vm_writer.write_label(lbl_end)
        self._compile_raw(f'</{CompilationEngine.XmlKeys.IF_STATEMENT.value}>')

    def compile_expression(self) -> None:
        """
        Compiles an expression
        :return:
        """
        self._compile_raw(f'<{CompilationEngine.XmlKeys.EXPRESSION.value}>')
        token, _ = self._token_type_pair()
        if token != ')' or token != ';':
            self.compile_term()
            if self._tokenizer.get_token() in CompilationEngine.Grammar.OP.value:
                while self._token_type_pair()[CompilationEngine.TOKEN_VALUE] in CompilationEngine.Grammar.OP.value:
                    symbol = self._tokenizer.get_token()
                    self._compile_op()
                    self.compile_term()
                    self._vm_writer.write_arithmetic(Command.map_op(symbol))
                    if self._tokenizer.get_token() not in CompilationEngine.Grammar.OP.value:
                        break
        self._compile_raw(f'</{CompilationEngine.XmlKeys.EXPRESSION.value}>')

    def compile_term(self) -> None:
        """
        Compiles a term This routine is faced with a slight difficulty when trying to decide between some of the
        alternative parsing rules. Specifically, if the current token is an identifier, the routing must distinguish
        between a variable, an array entry, and a subroutine call. A single look-ahead token,
        which may be one of: "[", "(", "."  suffices to distinguish between the three possibilities.
        Any other token is not part of ths term and should not be advanced over.
        :return:
        """
        token, token_t = self._token_type_pair()
        self._compile_raw(f'<{CompilationEngine.XmlKeys.TERM.value}>')
        if token_t == TokenType.INT_CONST:
            self._compile_integer_constant()
        elif token_t == TokenType.STRING_CONST:
            self._compile_string_constant()
        elif token_t == TokenType.KEYWORD:
            self._compile_keyword_constant()
        elif token_t == TokenType.IDENTIFIER and self._tokenizer.next_token() not in ['[', '.']:
            arg_var = self._tokenizer.get_token()
            self._compile_var_name(metadata={'context': 'used'})
            arg_segment = self._segment_of_kind.get(self._symbol_table.kind_of(arg_var))
            arg_index = self._symbol_table.index_of(arg_var)
            self._vm_writer.write_push(arg_segment, arg_index)
        elif token_t == TokenType.IDENTIFIER and self._tokenizer.next_token() == '[':
            ar_name = self._tokenizer.get_token()
            ar_seg = self._segment_of_kind.get(self._symbol_table.kind_of(ar_name))
            ar_seg_idx = self._symbol_table.index_of(ar_name)
            self._compile_var_name(metadata={'context': 'used'})
            self._compile_symbol()
            self.compile_expression()
            self._vm_writer.write_push(ar_seg, ar_seg_idx)
            self._vm_writer.write_arithmetic(Command.ADD)
            self._vm_writer.write_pop(Segment.POINTER, 1)
            self._vm_writer.write_push(Segment.THAT, 0)
            self._expect_token([']'], self._compile_symbol)
        elif token == '(':
            self._compile_symbol()
            self.compile_expression()
            self._expect_token([')'], self._compile_symbol)
        elif token in CompilationEngine.Grammar.UN_OP.value:
            symbol = self._tokenizer.get_token()
            self._compile_unary_op()
            self.compile_term()
            self._vm_writer.write_arithmetic(Command.map_un_op(symbol))
        else:
            self._compile_subroutine_call()
        self._compile_raw(f'</{CompilationEngine.XmlKeys.TERM.value}>')

    def compile_expression_list(self) -> int:
        """
        Compiles a (possibly empty) comma-separated list of expressions.
        :return:
        """
        self._compile_raw(f'<{CompilationEngine.XmlKeys.EXPRESSION_LIST.value}>')
        token, _ = self._token_type_pair()
        expressions = 0
        if token != ')':
            expressions += 1
            self.compile_expression()
            while self._token_type_pair()[CompilationEngine.TOKEN_VALUE] == ',':
                expressions += 1
                self._compile_symbol()
                self.compile_expression()
                if self._tokenizer.get_token() != ',':
                    break
        self._compile_raw(f'</{CompilationEngine.XmlKeys.EXPRESSION_LIST.value}>')
        return expressions

    def compiled_vm_code(self) -> str:
        """
        Returns compiled code in XML structure
        :return:
        """
        return self._vm_writer.compiled()

    def compiled_xml_struct(self) -> str:
        """
        Returns compiled code in XML structure
        :return:
        """
        return self._compiled_source
