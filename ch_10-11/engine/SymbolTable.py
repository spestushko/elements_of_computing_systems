from lib import IdentifierKind


class SymbolTable:

    @staticmethod
    def _get_basic_table_def() -> dict:
        """
        Provides a basic definition of a hash table needed to keep track of variables
        :return:
        """
        return {
            '_rows': 0,
            'name': [],
            'kind': [],
            'type': [],
            'index': []
        }

    @staticmethod
    def _add_class_meta(scope: dict) -> None:
        """
        Adds metadata to dictionary related to class scope
        :param scope:
        :return:
        """
        scope['_numb_static'] = 0
        scope['_numb_field'] = 0

    @staticmethod
    def _add_subroutine_meta(scope: dict) -> None:
        """
        Adds metadata to dictionary related to subroutine scope
        :param scope:
        :return:
        """
        scope['_numb_arg'] = 0
        scope['_numb_var'] = 0

    @staticmethod
    def _search(scope: dict, match_by: str, value: str,  target_column: str):
        """
        Searches passed in `scope`'s column defined by `match_by` for the `value` and if found, returns the value
        of the value in `target_column`. Returns None if match is not found.
        :param scope: dict representing class scope or subroutine scope
        :param match_by: column to search in
        :param value: value to search for in the column defined by `match_by`
        :param target_column: column to return value from
        :return:
        """
        row_num = scope['_rows']
        if row_num > 0:
            for i in range(row_num):
                if scope[match_by][i] == value:
                    return scope[target_column][i]
        return None

    def __init__(self):
        """
        Creates a new empty symbol table
        """
        self._class_scope = {}
        self._subroutine_scope = {}

        self._start_class()
        self.start_subroutine()

    def _start_class(self):
        """
        Starts a new class scope
        This method should only be called once when the SymbolTable instance is created
        :return:
        """
        # First do a clear to remove references (if any) on items in the dictionary
        self._class_scope.clear()
        self._class_scope = SymbolTable._get_basic_table_def()
        SymbolTable._add_class_meta(self._class_scope)

    def _find_attr(self, match_by: str, value: str, target_column: str):
        """
        Searches all scopes in column defined by `match_by` for the `value` and if found, returns the value
        of the value in `target_column`. Returns None if match is not found.
        :param match_by: column to search in
        :param value: value to search for in the column defined by `match_by`
        :param target_column: column to return value from
        :return:
        """
        # Searches subroutine scope first (if exists)
        result = SymbolTable._search(self._subroutine_scope, match_by, value, target_column)
        if result is not None:
            return result
        # If not exists in subroutine scope, search class scope next (returns None if no match is found)
        return SymbolTable._search(self._class_scope, match_by, value, target_column)

    def start_subroutine(self):
        """
        Starts a new subroutine scope (reset's subroutine's symbol table).

        Symbol table contents of identifier information:
          - Name
          - Kind (STATIC, FIELD, ARG, VAR) - which also knows the scope of the identifier
          - Type
          - Index (inc when symbol is added to the table, reset when starting a new scope)

          *** If an identifier is not found in the symbol table, assume its a subroutine name or a class name ***
          *** subroutine name or a class name are not kept in the symbol table ***
        :return:
        """
        # First do a clear to remove references (if any) on items in the dictionary
        self._subroutine_scope.clear()
        self._subroutine_scope = SymbolTable._get_basic_table_def()
        SymbolTable._add_subroutine_meta(self._subroutine_scope)

    def define(self, name: str, s_type: str, kind: IdentifierKind) -> None:
        """
        Defines a new identifier of a given name, type and kind and assigns it
        a running index. STATIC and FIELD identifiers have a class scope, while
        ARG and VAR identifiers have a subroutine scope.

        :param name: Symbol name
        :param s_type: int, char, boolean, className
        :param kind: Static, Field, Argument, Var
        :return:
        """
        if kind in [IdentifierKind.STATIC, IdentifierKind.FIELD]:
            self._class_scope['name'].append(name)
            self._class_scope['type'].append(s_type)
            self._class_scope['kind'].append(kind)
            self._class_scope['index'].append(self._class_scope[f'_numb_{kind.value}'])
            self._class_scope[f'_numb_{kind.value}'] += 1
            self._class_scope['_rows'] += 1
        elif kind in [IdentifierKind.VAR, IdentifierKind.ARGUMENT]:
            self._subroutine_scope['name'].append(name)
            self._subroutine_scope['type'].append(s_type)
            self._subroutine_scope['kind'].append(kind)
            self._subroutine_scope['index'].append(self._subroutine_scope[f'_numb_{kind.value}'])
            self._subroutine_scope[f'_numb_{kind.value}'] += 1
            self._subroutine_scope['_rows'] += 1
        else:
            raise ValueError(f'Compilation error: Invalid identifier kind {kind.value}')

    def var_count(self, kind: IdentifierKind) -> int:
        """
        Returns the number of variables of the given kind already defined in the
        current scope.
        :param kind:
        :return:
        """
        if kind in [IdentifierKind.STATIC, IdentifierKind.FIELD]:
            return self._class_scope[f'_numb_{kind.value}']
        elif kind in [IdentifierKind.VAR, IdentifierKind.ARGUMENT]:
            return self._subroutine_scope[f'_numb_{kind.value}']
        else:
            raise ValueError(f'Compilation error: Invalid identifier kind {kind.value}')

    def kind_of(self, name: str) -> IdentifierKind:
        """
        Returns the kind of the named identifier in the current scope. If the
        identifier is unknown in the current scope, returns None.
        :param name:
        :return:
        """
        return self._find_attr(match_by='name', value=name, target_column='kind')

    def type_of(self, name: str) -> str:
        """
        Returns the type of the named identifier in the current scope.
        :param name:
        :return:
        """
        return self._find_attr(match_by='name', value=name, target_column='type')

    def index_of(self, name: str) -> int:
        """
        Returns the index assigned to the named identifier
        :param name:
        :return:
        """
        return self._find_attr(match_by='name', value=name, target_column='index')