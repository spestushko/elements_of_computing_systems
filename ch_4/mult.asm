// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)
    @i      // refers to some mem loc
    M=1     // i = 1
    @2
    M=0
    // Check if first multiplier is zero, jump to end if true
    @0
    D=M
    @END
    D;JEQ
    // Check if second multiplier is zero, jump to end if true
    @1
    D=M
    @END
    D;JEQ
(LOOP)
    // Checks if loop needs to be stopped (end to determine if prod is calculated)
    @i
    D=M
    @1
    D=D-M
    @END
    D;JGT
    // Selects R0 register (base added to prod)
    @0
    D=M
    // Adds value in R0 register to prod result
    @2
    M=M+D
    // Increments counter
    @i
    M=M+1
    @LOOP
    0;JMP    // Goto LOOP
(END)
    @END
    0;JMP    // Infinite loop: terminate execution of Hack programs