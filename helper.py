def out(sf):
    for i in range(0, 16):
        print(sf.format(i, i, i, i))

def run():
    s_ab = '''
        And(a=in[{}], b=notsel, out=a[{}]);
        And(a=in[{}], b=sel, out=b[{}]);

    '''
    out(s_ab)

if __name__ == '__main__':
    run()
